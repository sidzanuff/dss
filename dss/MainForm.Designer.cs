﻿namespace dss
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.runButton = new System.Windows.Forms.Button();
            this.exeBox = new System.Windows.Forms.GroupBox();
            this.selectExeButton = new System.Windows.Forms.Button();
            this.exeTextBox = new System.Windows.Forms.TextBox();
            this.saveBox = new System.Windows.Forms.GroupBox();
            this.selectSaveButton = new System.Windows.Forms.Button();
            this.saveTextBox = new System.Windows.Forms.TextBox();
            this.backupButton = new System.Windows.Forms.Button();
            this.restoreButton = new System.Windows.Forms.Button();
            this.commentTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.saveCommentButton = new System.Windows.Forms.Button();
            this.saveGraph = new dss.GraphControl();
            this.panel1.SuspendLayout();
            this.exeBox.SuspendLayout();
            this.saveBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.saveGraph);
            this.panel1.Location = new System.Drawing.Point(12, 159);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(600, 364);
            this.panel1.TabIndex = 0;
            // 
            // runButton
            // 
            this.runButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.runButton.Location = new System.Drawing.Point(512, 130);
            this.runButton.Name = "runButton";
            this.runButton.Size = new System.Drawing.Size(100, 23);
            this.runButton.TabIndex = 1;
            this.runButton.Text = "Run game";
            this.runButton.UseVisualStyleBackColor = true;
            this.runButton.Click += new System.EventHandler(this.runButton_Click);
            // 
            // exeBox
            // 
            this.exeBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.exeBox.Controls.Add(this.selectExeButton);
            this.exeBox.Controls.Add(this.exeTextBox);
            this.exeBox.Location = new System.Drawing.Point(15, 12);
            this.exeBox.Name = "exeBox";
            this.exeBox.Size = new System.Drawing.Size(597, 53);
            this.exeBox.TabIndex = 2;
            this.exeBox.TabStop = false;
            this.exeBox.Text = "Game executable";
            this.exeBox.DragDrop += new System.Windows.Forms.DragEventHandler(this.exeBox_DragDrop);
            this.exeBox.DragEnter += new System.Windows.Forms.DragEventHandler(this.exeBox_DragEnter);
            // 
            // selectExeButton
            // 
            this.selectExeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.selectExeButton.Location = new System.Drawing.Point(516, 17);
            this.selectExeButton.Name = "selectExeButton";
            this.selectExeButton.Size = new System.Drawing.Size(75, 23);
            this.selectExeButton.TabIndex = 1;
            this.selectExeButton.Text = "Select";
            this.selectExeButton.UseVisualStyleBackColor = true;
            this.selectExeButton.Click += new System.EventHandler(this.selectExeButton_Click);
            // 
            // exeTextBox
            // 
            this.exeTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.exeTextBox.Location = new System.Drawing.Point(6, 19);
            this.exeTextBox.Name = "exeTextBox";
            this.exeTextBox.Size = new System.Drawing.Size(504, 20);
            this.exeTextBox.TabIndex = 0;
            // 
            // saveBox
            // 
            this.saveBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.saveBox.Controls.Add(this.selectSaveButton);
            this.saveBox.Controls.Add(this.saveTextBox);
            this.saveBox.Location = new System.Drawing.Point(15, 71);
            this.saveBox.Name = "saveBox";
            this.saveBox.Size = new System.Drawing.Size(597, 53);
            this.saveBox.TabIndex = 3;
            this.saveBox.TabStop = false;
            this.saveBox.Text = "Save file";
            this.saveBox.DragDrop += new System.Windows.Forms.DragEventHandler(this.saveBox_DragDrop);
            this.saveBox.DragEnter += new System.Windows.Forms.DragEventHandler(this.saveBox_DragEnter);
            // 
            // selectSaveButton
            // 
            this.selectSaveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.selectSaveButton.Location = new System.Drawing.Point(516, 17);
            this.selectSaveButton.Name = "selectSaveButton";
            this.selectSaveButton.Size = new System.Drawing.Size(75, 23);
            this.selectSaveButton.TabIndex = 1;
            this.selectSaveButton.Text = "Select";
            this.selectSaveButton.UseVisualStyleBackColor = true;
            this.selectSaveButton.Click += new System.EventHandler(this.selectSaveButton_Click);
            // 
            // saveTextBox
            // 
            this.saveTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.saveTextBox.Location = new System.Drawing.Point(6, 19);
            this.saveTextBox.Name = "saveTextBox";
            this.saveTextBox.Size = new System.Drawing.Size(504, 20);
            this.saveTextBox.TabIndex = 0;
            // 
            // backupButton
            // 
            this.backupButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.backupButton.Location = new System.Drawing.Point(406, 130);
            this.backupButton.Name = "backupButton";
            this.backupButton.Size = new System.Drawing.Size(100, 23);
            this.backupButton.TabIndex = 4;
            this.backupButton.Text = "Backup save";
            this.backupButton.UseVisualStyleBackColor = true;
            this.backupButton.Click += new System.EventHandler(this.backupButton_Click);
            // 
            // restoreButton
            // 
            this.restoreButton.Location = new System.Drawing.Point(15, 130);
            this.restoreButton.Name = "restoreButton";
            this.restoreButton.Size = new System.Drawing.Size(100, 23);
            this.restoreButton.TabIndex = 5;
            this.restoreButton.Text = "Restore save";
            this.restoreButton.UseVisualStyleBackColor = true;
            this.restoreButton.Click += new System.EventHandler(this.restoreButton_Click);
            // 
            // commentTextBox
            // 
            this.commentTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.commentTextBox.Location = new System.Drawing.Point(72, 529);
            this.commentTextBox.Name = "commentTextBox";
            this.commentTextBox.Size = new System.Drawing.Size(459, 20);
            this.commentTextBox.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 532);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Comment:";
            // 
            // saveCommentButton
            // 
            this.saveCommentButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.saveCommentButton.Location = new System.Drawing.Point(537, 527);
            this.saveCommentButton.Name = "saveCommentButton";
            this.saveCommentButton.Size = new System.Drawing.Size(75, 23);
            this.saveCommentButton.TabIndex = 8;
            this.saveCommentButton.Text = "Save";
            this.saveCommentButton.UseVisualStyleBackColor = true;
            this.saveCommentButton.Click += new System.EventHandler(this.saveCommentButton_Click);
            // 
            // saveGraph
            // 
            this.saveGraph.Current = 0;
            this.saveGraph.Location = new System.Drawing.Point(3, 3);
            this.saveGraph.Name = "saveGraph";
            this.saveGraph.Selected = null;
            this.saveGraph.Size = new System.Drawing.Size(150, 150);
            this.saveGraph.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 561);
            this.Controls.Add(this.saveCommentButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.commentTextBox);
            this.Controls.Add(this.restoreButton);
            this.Controls.Add(this.backupButton);
            this.Controls.Add(this.saveBox);
            this.Controls.Add(this.exeBox);
            this.Controls.Add(this.runButton);
            this.Controls.Add(this.panel1);
            this.MinimumSize = new System.Drawing.Size(400, 300);
            this.Name = "Form1";
            this.Text = "Duo\'s Save Scummer";
            this.Load += new System.EventHandler(this.Form1_Load_1);
            this.panel1.ResumeLayout(false);
            this.exeBox.ResumeLayout(false);
            this.exeBox.PerformLayout();
            this.saveBox.ResumeLayout(false);
            this.saveBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private GraphControl saveGraph;
        private System.Windows.Forms.Button runButton;
        private System.Windows.Forms.GroupBox exeBox;
        private System.Windows.Forms.Button selectExeButton;
        private System.Windows.Forms.TextBox exeTextBox;
        private System.Windows.Forms.GroupBox saveBox;
        private System.Windows.Forms.Button selectSaveButton;
        private System.Windows.Forms.TextBox saveTextBox;
        private System.Windows.Forms.Button backupButton;
        private System.Windows.Forms.Button restoreButton;
        private System.Windows.Forms.TextBox commentTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button saveCommentButton;
    }
}