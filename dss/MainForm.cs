﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace dss
{
    public partial class MainForm : Form
    {
        private const string CONFIG_FILE_NAME = "dss.xml";

        private Config config;

        public MainForm()
        {
            InitializeComponent();

            exeBox.AllowDrop = true;
            saveBox.AllowDrop = true;

            saveGraph.OnSelected += saveGraph_OnSelected;

            LoadConfig();

            exeTextBox.Text = config.ExecutablePath;
            saveTextBox.Text = config.SaveFilePath;
        }

        private void LoadConfig()
        {
            if (File.Exists(CONFIG_FILE_NAME))
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(Config));
                using (Stream stream = File.OpenRead(CONFIG_FILE_NAME))
                {
                    config = (Config)xmlSerializer.Deserialize(stream);
                }
            }
            else
            {
                config = new Config();
            }
        }

        private void SaveConfig()
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(Config));
            using (Stream stream = new FileStream(CONFIG_FILE_NAME, FileMode.Create))
            {
                xmlSerializer.Serialize(stream, config);
            }
        }

        private void saveGraph_OnSelected(object sender, EventArgs e)
        {
            commentTextBox.Text = saveGraph.Selected.Comment;
        }

        private void Form1_Load_1(object sender, EventArgs e)
        {
            saveGraph.SetItems(config.SaveGames);

            if (config.SaveGames.Count > 0)
            {
                saveGraph.Selected = config.SaveGames.FirstOrDefault(sg => sg.Id == config.Current);
                saveGraph.Current = config.Current;
            }

            saveGraph.RefreshGraph();
        }

        private void exeBox_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
        }

        private void exeBox_DragDrop(object sender, DragEventArgs e)
        {
            config.ExecutablePath = ((string[])e.Data.GetData(DataFormats.FileDrop))[0];
            exeTextBox.Text = config.ExecutablePath;
            SaveConfig();
        }

        private void saveBox_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
        }

        private void saveBox_DragDrop(object sender, DragEventArgs e)
        {
            config.SaveFilePath = ((string[])e.Data.GetData(DataFormats.FileDrop))[0];
            saveTextBox.Text = config.SaveFilePath;
            SaveConfig();
        }

        private void selectExeButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                config.ExecutablePath = dialog.FileName;
                exeTextBox.Text = config.ExecutablePath;
                SaveConfig();
            }
        }

        private void selectSaveButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                config.SaveFilePath = dialog.FileName;
                saveTextBox.Text = config.SaveFilePath;
                SaveConfig();
            }
        }

        private void restoreButton_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you really want to restore selected save?", "", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.OK)
            {
                File.Copy(GetSavePath(saveGraph.Selected.Id), config.SaveFilePath, true);
                config.Current = saveGraph.Selected.Id;
                saveGraph.Current = saveGraph.Selected.Id;
                SaveConfig();
                saveGraph.RefreshGraph();
            }
        }

        private void backupButton_Click(object sender, EventArgs e)
        {
            int id;

            if (config.SaveGames.Any())
            {
                id = config.SaveGames.Max(sg => sg.Id) + 1;
            }
            else
            {
                id = 1;
            }

            SaveGame newSave = new SaveGame
            {
                Id = id,
                ParentId = config.Current,
                Date = DateTime.Now
            };

            config.SaveGames.Add(newSave);

            File.Copy(config.SaveFilePath, GetSavePath(id), true);

            config.Current = id;
            SaveConfig();

            saveGraph.Current = id;
            saveGraph.Selected = newSave;
            saveGraph.RefreshGraph();
        }

        private string GetSavePath(int id)
        {
            string dir = Path.Combine(Directory.GetCurrentDirectory(), "savegames");
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            return Path.Combine(dir, id.ToString());
        }

        private void runButton_Click(object sender, EventArgs e)
        {
            ProcessStartInfo proc = new ProcessStartInfo(exeTextBox.Text);
            Process.Start(proc);
        }

        private void saveCommentButton_Click(object sender, EventArgs e)
        {
            if (saveGraph.Selected != null)
            {
                saveGraph.Selected.Comment = commentTextBox.Text;
                SaveConfig();
                saveGraph.RefreshGraph();
            }
        }
    }
}