﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace dss
{
    public class SaveGame
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public DateTime Date { get; set; }
        public string Comment { get; set; }
    }
}
