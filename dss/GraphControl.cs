﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace dss
{
    public partial class GraphControl : UserControl
    {
        private const int X_STEP = 20;
        private const int ROW_HEIGHT = 20;
        private const int DOT_SIZE = 6;
        private const int LABEL_WIDTH = 50;

        private List<SaveGame> nodes;
        private SaveGame selected;
        private Dictionary<SaveGame, int> parentOffsets = new Dictionary<SaveGame, int>();

        private Stack<SaveGame> queue = new Stack<SaveGame>();
        private int x;

        public event EventHandler OnSelected;

        public GraphControl()
        {
            InitializeComponent();

            DoubleBuffered = true;
        }

        public void SetItems(List<SaveGame> items)
        {
            this.nodes = items;
        }

        public SaveGame Selected
        {
            get
            {
                return selected;
            }
            set
            {
                this.selected = value;
                Invalidate();
            }
        }

        public int Current { get; set; }

        public void RefreshGraph()
        {
            Height = ROW_HEIGHT * nodes.Count;
            Invalidate();
        }

        private void GraphControl_Load(object sender, EventArgs e)
        {

        }

        private void GraphControl_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.Clear(SystemColors.Window);

            if (nodes == null)
            {
                return;
            }

            e.Graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            e.Graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;

            if (selected != null)
            {
                int y = GetY(selected);
                e.Graphics.FillRectangle(new SolidBrush(SystemColors.Highlight), 0, y, Width, ROW_HEIGHT);
            }

            SaveGame node = nodes.FirstOrDefault();
            x = LABEL_WIDTH;

            while (true)
            {
                DrawPath(node, e.Graphics);

                if (queue.Any())
                {
                    node = queue.Pop();
                    SaveGame parent = GetParent(node);
                    int y = GetY(parent);
                    e.Graphics.DrawLine(Pens.Black, parentOffsets[parent], y + ROW_HEIGHT / 2, x + X_STEP, y + ROW_HEIGHT / 2);
                    x += X_STEP;
                }
                else
                {
                    break;
                }
            }

            x += X_STEP;
            int maxX = x;

            foreach (SaveGame sg in nodes)
            {
                Font font = sg.Id == Current ? new Font(this.Font, FontStyle.Bold) : this.Font;

                string label = sg.Date.ToString() + " " + sg.Comment;
                SizeF size = e.Graphics.MeasureString(label, font);
                e.Graphics.DrawString(label, font, Brushes.Black, x, GetY(sg) + ROW_HEIGHT / 2 - size.Height / 2);
                maxX = (int)Math.Max(maxX, x + size.Width);
            }

            if (Width < maxX)
            {
                Width = maxX;
            }
        }

        private void DrawPath(SaveGame node, Graphics graphics)
        {
            while(node != null)
            {
                parentOffsets[node] = x;
                DrawNode(node, graphics);
                node = GetChildren(node);
            }
        }

        private SaveGame GetChildren(SaveGame current)
        {
            var children = nodes.Where(n => n.ParentId == current.Id);
            children.Skip(1).ToList().ForEach(queue.Push);
            return children.FirstOrDefault();
        }

        private void DrawNode(SaveGame node, Graphics graphics)
        {
            int y = GetY(node);

            string label = node.Id.ToString();

            graphics.DrawRectangle(Pens.Gray, 0, y, Width, ROW_HEIGHT);
            Font font = node.Id == Current ? new Font(this.Font, FontStyle.Bold) : this.Font;
            graphics.DrawString(label, font, Brushes.Black, 5, y + ROW_HEIGHT / 2 - graphics.MeasureString(label, Font).Height / 2);
            graphics.FillEllipse(Brushes.Red, x - DOT_SIZE / 2, y + ROW_HEIGHT / 2 - DOT_SIZE / 2, DOT_SIZE, DOT_SIZE);

            SaveGame parent = GetParent(node);
            if (parent != null)
            {
                int parentY = GetY(parent);
                graphics.DrawLine(Pens.Black, x, y + ROW_HEIGHT / 2, x, parentY + ROW_HEIGHT / 2);
            }
        }

        private int GetY(SaveGame current)
        {
            return nodes.IndexOf(current) * ROW_HEIGHT;
        }

        private SaveGame GetParent(SaveGame node)
        {
            return nodes.FirstOrDefault(n => n.Id == node.ParentId); 
        }

        private void GraphControl_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                int index = e.Y / ROW_HEIGHT;
                Selected = nodes[index];

                OnSelected(this, EventArgs.Empty);
            }
        }
    }
}
