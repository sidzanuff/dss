﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace dss
{
    public class Config
    {
        public List<SaveGame> SaveGames { get; set; }
        public int Current { get; set; }
        public string ExecutablePath { get; set; }
        public string SaveFilePath { get; set; }

        public Config()
        {
            SaveGames = new List<SaveGame>();
        }
    }
}
